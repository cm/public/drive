init =
{
  nodeGroups = [ botleft, top, botright ];

  mesh = 
  {
    type = geo;
    file = house.geom;
  };

  botleft = [0];
  top = [2];
  botright = [4];
};

model =
{
  type = Multi;

  models = [ frame, diri ];

  frame =
  {
    type = Frame;

    elements = all;

    subtype = linear;

    EA = 10.e6;
    GAs = 10.e6;
    EI = 10.e3;
    Mp = 3.e3;

    shape =
    {
      type = Line2;
      intScheme = Gauss1;
    };
  };

  diri =
  {
    type = Dirichlet; 

    groups = [ botleft, botleft, botleft, botright, botright, botright, top ];
    dofs   = [ dx, dy, phi, dx, dy, phi, dy ];
    values = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.001 ];
    dispIncr = [ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.001 ];
  };
};

solver =
{
  type = Solver;
};

frameview =
{
  type = FrameView;
  deform = 1.;
  interactive = False;
  plotStress = M;
};

loaddisp = 
{
  type = LoadDisp;
  groups = [ top ];
};

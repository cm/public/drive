init =
{
  type = Init;

  mesh =
  {
    type = gmsh;
    file = square.msh;
  };

  nodeGroups = [ left, right, bottom ];

  left =
  {
    xtype = min;
  };

  right =
  {
    xtype = max;
  };
  
  bottom =
  {
    ytype = min;
  };
};

model =
{
  type = Multi;

  models = [ solid, fixed, neumann ];

  solid =
  {
    type = Solid;

    elements = all;

    material =
    {
      type = J2;
      rank = 2;
      anmodel = plane_stress;

      E = 1000.;
      nu = 0.2;

      yield = 50 + 120*kappa;
      maxIter = 1000;
      tolerance = 1.e-7;
    };

    thickness = 1.0;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  dispcontrol =
  {
    type = Dirichlet;

    groups = [ left, bottom, right ];
    dofs   = [ dx  , dy,     dx    ];
    values = [ 0.  , 0.,     0.02  ];
    timeSignal = t;
    deltaTime = 1.0;
  };
  
  fixed =
  {
    type = Dirichlet;

    groups = [ left, bottom ];
    dofs   = [ dx  , dy     ];
    values = [ 0.  , 0.     ];
  };
  
  neumann =
  {
    type = Neumann;
    
    groups = [ right ];
    dofs   = [ dx    ];
    values = [ 5.0   ];
    timeSignal = t;
    deltaTime = 1.0;
  };
};

nonlin =
{
  type = Nonlin;
  nsteps = 20;
  itermax = 100;
  tolerance = 1e-6;
  modified = False;
};

lodi =
{
  type = LoadDisp;
  groups = [right];
};

graph =
{
  xData = [lodi.right.disp.dx];
  yData = [lodi.right.load.dx];
  xlabel = u;
  ylabel = F;
};
init =
{
  mesh =
  {
    type = gmsh;
    file = dogbone.msh;
  };

  nodeGroups = [ left, right ];

  left = 
  {
    xtype = min;
  };

  right = 
  {
    xtype = max;
  };
};

model =
{
  type = Multi;

  models = [ solid, diri ];

  solid =
  {
    type = Solid;
    elements = all;

    material =
    {
      type = FE2;
      rank = 2;
      filename = micro.pro;
    };

    thickness = 1.;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  diri =
  {
    type = Dirichlet;
    groups = [ left, left, right ];
    dofs = [ dy, dx, dx ];
    values = [ 0., 0., 0. ];
    dispIncr = [ 0., 0., 0.01 ];
  };
};

solver =
{
  type = Nonlin;
  nsteps = 4;
  itermax = 20;
  tolerance = 1e-8;
};

lodi =
{
  type = LoadDisp;
  groups = [left, right];
};


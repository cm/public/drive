init =
{
  mesh =
  {
    type = gmsh;
    file = unitCell.msh;
  };
};

model =
{
  type = Multi;

  models = [ fiber, matrix, linear ];

  fiber =
  {
    type = Solid;
    elements = fiber;

    material =
    {
      type = Elastic;
      rank = 2;
      anmodel = plane_stress;

      E = 74000.;
      nu = 0.2;
    };

    thickness = 1.;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  matrix =
  {
    type = Solid;
    elements = matrix;

    thickness = 1.;

    material =
    {
      type = J2;
      rank = 2;
      anmodel = plane_stress;

      E = 3000.;
      nu = 0.2;
      rho = 1.;

      yield = 64.8 - 33.6*exp(kappa/-0.003407);
      maxIter = 1000;
      tolerance = 1.e-7;
    };

    thickness = 1.;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  linear =
  {
    type = LinearBC;
    strainKey = macrostrain;
  };
};

solver =
{
  type = Nonlin;
  nsteps = 4;
  itermax = 20;
  tolerance = 1e-8;
};

lodi =
{
  type = LoadDisp;
  groups = [left, right, top, bottom];
};

homogenization =
{
  type = Homogenization;
};


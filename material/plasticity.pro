init =
{
  type = Init;

  mesh =
  {
    type = gmsh;
    file = voids.msh;
  };

  nodeGroups = [ left, right, bottom ];

  left =
  {
    xtype = min;
  };

  right =
  {
    xtype = max;
  };
  
  bottom =
  {
    ytype = min;
  };
};

model =
{
  type = Multi;

  models = [ solid, dispcontrol ];

  solid =
  {
    type = Solid;

    elements = all;

    material =
    {
      type = J2;
      rank = 2;
      anmodel = plane_stress;

      E = 1000.;
      nu = 0.2;

      yield = 80+10*kappa;
      maxIter = 1000;
      tolerance = 1.e-10;
    };

    thickness = 1.0;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  dispcontrol =
  {
    type = Dirichlet;

    groups = [ left, right, bottom ];
    dofs   = [ dx  , dx   , dy     ];
    values = [ 0.  , 0.02 , 0.     ];
    timeSignal = t;
    deltaTime = 1.0;
  };
};

nonlin =
{
  type = Nonlin;
  nsteps = 40;
  itermax = 100;
  tolerance = 1e-6;
  modified = False;
};

lodi =
{
  type = LoadDisp;
  groups = [right];
};

graph =
{
  xData = [lodi.right.disp.dx];
  yData = [lodi.right.load.dx];
  xlabel = Displacement [mm];
  ylabel = Load [N];
};

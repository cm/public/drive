init =
{
  nodeGroups = [ left, right ];

  mesh =
  {
    type = gmsh;
    file = mesh.msh;
  };

  left =
  {
    xtype = min;
  };

  right =
  {
    xtype = max;
  };
};

model =
{
  type = Multi;

  models = [ pde, diri, neum ];

  pde =
  {
    type = Poisson;
    elements = all;

    kappa = 10.0;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  diri =
  {
    type = Dirichlet;

    groups = [ left ];
    dofs   = [ u ];
    values = [ 0.0 ];
  };

  neum =
  {
    type = Neumann;

    groups = [ right ];
    dofs   = [ u ];
    values = [ 1.0 ];
  };
};

solver =
{
  type = Solver;
  nsteps = 1;
};

view = 
{
  plot = solution[u];
  ncolors = 10;
  interactive = False;
  colorMap = plasma_r;
};


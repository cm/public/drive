init =
{
  nodeGroups = [ left, right ];

  mesh =
  {
    type = gmsh;
    file = mesh.msh;
  };

  left =
  {
    xtype = min;
  };

  right =
  {
    xtype = max;
  };
};

model =
{
  type = Multi;

  models = [ pde, diri, neum ];

  pde =
  {
    type = Diffusion;
    elements = all;

    kappa = 10.0;
    rho = 1.0;
    initialValue = 0.0;

    shape =
    {
      type = Triangle3;
      intScheme = Gauss1;
    };
  };

  diri =
  {
    type = Dirichlet;

    groups = [ left ];
    dofs   = [ u ];
    values = [ 0.0 ];
  };

  neum =
  {
    type = Neumann;

    groups = [ right ];
    dofs   = [ u ];
    values = [ 1.0 ];
    timeSignal = min(t,1.0);
    deltaTime = 0.01;
  };
};

solver =
{
  type = Trapezoidal;
  deltaTime = 0.01;
  nsteps = 200;
  theta = 0.0;
};

view = 
{
  plot = solution[u];
  ncolors = 10;
  interactive = True;
  colorMap = plasma_r;
};
